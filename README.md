# ScriptsHub
Multi-purpose PowerShell script to remotely run commands on networked computers.

*Script must be run as an administrator! Do this by right-clicking PowerShell and selecting "Run as administrator".*

Functions include:
- Collecting a diagnostic report on PC's:
    - Options for what you want included in report.
    - Emailed to as many people as you want.
    - Report generated in nice HTML page.
        - Tables.
        - Collapsable sections.
        - Color coding.
- Changing power management settings on other machines.
- Stress tests for CPU, RAM, and network conditions on machines. Displays comparisons against baseline benchmarks.
