﻿#Enter-PSSession -ComputerName CAE-D-MJ03P5EA -Credential username
#New-PSDrive -Name Scripts -PSProvider FileSystem -Root \\caeisi01\Public\Support\Scripts\Desktop\HTMLSystemInformation.ps1

#New-PSDrive –Name "L" -PSProvider FileSystem -Root "\\caeisi01\Public" 
#invoke-command -computername CAE-D-MG000B0B -filepath \\caeisi01\Public\Support\Scripts\Desktop\HTMLSystemInformation.ps1
#

#### HTML Output Formatting advanced features #######
# Save-Module -Name ReportHTML -Path c:\support


################################################################################################
#### Functions ####
################################################################################################


Function Set-CellColor
{   
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory,Position=0)]
        [string]$Property,
        [Parameter(Mandatory,Position=1)]
        [string]$Color,
        [Parameter(Mandatory,ValueFromPipeline)]
        [Object[]]$InputObject,
        [Parameter(Mandatory)]
        [string]$Filter,
        [switch]$Row
    )
    
    Begin {
        Write-Verbose "$(Get-Date): Function Set-CellColor begins"
        If ($Filter)
        {   If ($Filter.ToUpper().IndexOf($Property.ToUpper()) -ge 0)
            {   $Filter = $Filter.ToUpper().Replace($Property.ToUpper(),"`$Value")
                Try {
                    [scriptblock]$Filter = [scriptblock]::Create($Filter)
                }
                Catch {
                    Write-Warning "$(Get-Date): ""$Filter"" caused an error, stopping script!"
                    Write-Warning $Error[0]
                    Exit
                }
            }
            Else
            {   Write-Warning "Could not locate $Property in the Filter, which is required.  Filter: $Filter"
                Exit
            }
        }
    }
    
    Process {
        ForEach ($Line in $InputObject)
        {   If ($Line.IndexOf("<tr><th") -ge 0)
            {   Write-Verbose "$(Get-Date): Processing headers..."
                $Search = $Line | Select-Object-String -Pattern '<th ?[a-z\-:;"=]*>(.*?)<\/th>' -AllMatches
                $Index = 0
                ForEach ($Match in $Search.Matches)
                {   If ($Match.Groups[1].Value -eq $Property)
                    {   Break
                    }
                    $Index ++
                }
                If ($Index -eq $Search.Matches.Count)
                {   Write-Warning "$(Get-Date): Unable to locate property: $Property in table header"
                    Exit
                }
                Write-Verbose "$(Get-Date): $Property column found at index: $Index"
            }
            If ($Line -match "<tr( style=""background-color:.+?"")?><td")
            {   $Search = $Line | Select-Object-String -Pattern '<td ?[a-z\-:;"=]*>(.*?)<\/td>' -AllMatches
                $Value = $Search.Matches[$Index].Groups[1].Value -as [double]
                If (-not $Value)
                {   $Value = $Search.Matches[$Index].Groups[1].Value
                }
                If (Invoke-Command $Filter)
                {   If ($Row)
                    {   Write-Verbose "$(Get-Date): Criteria met!  Changing row to $Color..."
                        If ($Line -match "<tr style=""background-color:(.+?)"">")
                        {   $Line = $Line -replace "<tr style=""background-color:$($Matches[1])","<tr style=""background-color:$Color"
                        }
                        Else
                        {   $Line = $Line.Replace("<tr>","<tr style=""background-color:$Color"">")
                        }
                    }
                    Else
                    {   Write-Verbose "$(Get-Date): Criteria met!  Changing cell to $Color..."
                        $Line = $Line.Replace($Search.Matches[$Index].Value,"<td style=""background-color:$Color"">$Value</td>")
                    }
                }
            }
            Write-Output $Line
        }
    }
    
    End {
        Write-Verbose "$(Get-Date): Function Set-CellColor completed"
    }
}

Function Set-AlternatingRows {
    [CmdletBinding()]
   	Param(
       	[Parameter(Mandatory,ValueFromPipeline)]
        [string]$Line,
       
   	    [Parameter(Mandatory)]
       	[string]$CSSEvenClass,
       
        [Parameter(Mandatory)]
   	    [string]$CSSOddClass
   	)
	
Begin {
	Write-Host running setcss function
    	$ClassName = $CSSEvenClass
	}
	Process {
		If ($Line.Contains("<tr><td>"))
		{	$Line = $Line.Replace("<tr>","<tr class=""$ClassName"">")
			If ($ClassName -eq $CSSEvenClass)
			{	$ClassName = $CSSOddClass
			}
			Else
			{	$ClassName = $CSSEvenClass
			}
		}
		Return $Line
	}
}


function Get-LoggedOnUser
{
  param([String[]]$ComputerName = $env:COMPUTERNAME)

    $ComputerName | ForEach-Object {
      (quser /SERVER:$_) -replace '\s{2,}', ',' | 
        ConvertFrom-CSV |
        Add-Member -MemberType NoteProperty -Name ComputerName -Value $_ -PassThru
  }
} 


$a = @" 
<style>BODY{background-color:LightGray; font-family:Calibri;font-size:12pt;}
TABLE{border-width: 1px;border-style: solid;font-family:Calibri;font-size:10pt;border-color: black;border-collapse: collapse;}
TH{border-width: 1px;padding: 5px;border-style: solid;border-color: black;background-color:CadetBlue}
TD{border-width: 1px;padding: 5px;border-style: solid;border-color: black;background-color:tan}
.odd  { background-color:#ffffff; }.even { background-color:#dddddd; }</style>
"@

################################################################################################
#### Global variables ####
################################################################################################
# Get Start Time
$startDTM = (Get-Date)
$ErrorActionPreference = 'SilentlyContinue'
$vUserName = (Get-Item env:\username).Value 			## This will get username using environment variable
$filepath = "C:\Support"		## this is user profile  using environment variable
$URL1= "http://one.nmrs.com/Pages/Firm%20View.aspx"
$URL2= "https://app.chromeriver.com/login"
$vComputerName=$null
$User4Computer=$null

################################################################################################
#### Get Imputs ####
################################################################################################

Clear-Host
    Write-Host ' '
    Write-Host 'Input the User Name. This will retrieve the last logged in computer for the user.  Hit enter if you want to specify the computer name manually.' -ForegroundColor Yellow -BackgroundColor Black
    Write-Host ' '

$User4Computer = Read-Host

if ($User4Computer -eq '') {
    write-host 'No Imput Detected, Skipping SQL Query'
}
Else {
    Set-Location SQLSERVER:\SQL\CAEDB20\DEFAULT\Databases\Computer4User
    Invoke-sqlcmd "Select-Object DB_Name() AS Computer4user;"
    #Invoke-sqlcmd -query "Select-Object TOP 1 ComputerName FROM Computer4User Where-Object UserID = 'wbryan' ORDER BY DateUpdated DESC" -Verbose
    $SQLvComputerName=Invoke-sqlcmd -query "Select-Object TOP 1 ComputerName FROM Computer4User Where-Object UserID = '$User4Computer' ORDER BY DateUpdated DESC" -Verbose
    $vComputerName= $SQLvComputerName | Select-Object-Object -ExpandProperty ComputerName
    write-host $vComputerName - Retrieved from SQL Table -ForegroundColor Black -BackgroundColor Green 
}

if ($vComputerName -eq $null) {
    Write-Host ' '
    Write-Host Input the Computer Name -ForegroundColor Yellow -BackgroundColor Black
    Write-Host ' '
    $vComputerName = Read-Host
}   
if ($vComputerName -eq '') {
    (Write-Host No Computer Name Retrieved!  -ForegroundColor Yellow -BackgroundColor Red )
    Break Script
}

If (-not (Test-Connection -Computername $vComputerName -BufferSize 16 -Count 1 -Quiet)){  
    (Write-Host $vComputerName is Not online!  -ForegroundColor Yellow -BackgroundColor Red )
    Break Script
}

Else {
    (Write-Host $vComputerName is responding.  -ForegroundColor Black -BackgroundColor Green ) 
}


    Write-Host ' '
    Write-Host 'Enter the smtp address to send the report to.' -ForegroundColor Yellow -BackgroundColor Black
    Write-Host ' '
$to = Read-Host 
    Write-Host ' '
    Write-Host ' '
    Write-Host 'Enter Number of days to go back and gather logs. Start with 2.' -ForegroundColor Yellow -BackgroundColor Black
    Write-Host ' '
$days = Read-Host 
    Write-Host ' '
$date = (Get-Date).AddDays(-$days)


################################################
#  Get Logged on User
#################################################

IF ([string]::IsNullOrWhitespace($vComputerName)){$vComputerName = (Get-Item env:\Computername).Value} else {}
ConvertTo-Html -Title "System Information for $vComputerName" -Body "<h1> Computer Name : $vComputerName </h1>" >  "$filepath\$vComputerName.html" 
$ComputerOwner = Get-LoggedOnUser -Computername $vComputerName | Select-Object-Object -Property UserName
Get-LoggedOnUser -Computername $vComputerName | Select-Object-Object -Property UserName, SessionName, 'Logon Time', 'Idle Time' `
    | ConvertTo-html  -Body "<H2> Machine User </H2>" | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

$computeruser = Get-LoggedOnUser -Computername $vComputerName | Select-Object-Object -Property UserName, SessionName, 'Logon Time', 'Idle Time' -EA SilentlyContinue


################################################
#  Get Image Version and Bios
#################################################

Invoke-Command -cn $vComputerName -ScriptBlock {Get-ItemProperty HKLM:\Software\* | Select-Object-Object CaptureDate, Version, BuildDate} | Select-Object-Object -ExcludeProperty PSComputerName, RunspaceId | Where-Object-Object {!(($_.Version -eq $null))} `
    | ConvertTo-html -Head $a -Body "<H2> Image Version, Last Reboot and Free Memory </H2>" | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

get-WmiObject win32_operatingsystem -ComputerName $vComputerName | Select-Object Caption,Organization,@{LABEL='InstallDate';EXPRESSION={$_.ConverttoDateTime($_.InstallDate)}},OSArchitecture,Version,SerialNumber,BootDevice,WindowsDirectory,CountryCode `
    | ConvertTo-html | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

Get-WmiObject win32_bios -ComputerName $vComputerName | Select-Object Status,Version,PrimaryBIOS,Manufacturer,@{LABEL='ReleaseDate';EXPRESSION={$_.ConverttoDateTime($_.ReleaseDate)}},SerialNumber `
    | ConvertTo-html | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue
    #| ConvertTo-html -Body "<H2> BIOS Information</H2>" >>  "$filepath\$vComputerName.html" 
    

################################################
#  Get last reboot, Free Memory, Processor and Disk
#################################################
write-host "Getting Hardware Information"  -ForegroundColor Yellow -BackgroundColor DarkBlue
$sw = [Diagnostics.Stopwatch]::StartNew()

Get-WmiObject -Class win32_operatingsystem -ComputerName $vComputerName | Select-Object-Object FreePhysicalMemory, @{LABEL='LastBootUpTime';EXPRESSION={$_.ConverttoDateTime($_.lastbootuptime)}} `
    | ConvertTo-html | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

Get-WmiObject -Class Win32_ComputerSystem -ComputerName $vComputerName | Select-Object Status,Manufacturer,SystemFamily,TotalPhysicalMemory `
    | ConvertTo-html | Set-CellColor TotalPhysicalMemory red -Filter "TotalPhysicalMemory -lt 7000000000" | Set-CellColor TotalPhysicalMemory green -Filter "TotalPhysicalMemory -gt 7000000000" `
    | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

Get-WmiObject win32_logicalDisk -ComputerName $vComputerName | Select-Object DeviceID,VolumeName,Freespace,@{Expression={$_.Size /1Gb -as [int]};Label="Total Size - GB"},@{Expression={$_.Freespace / 1Gb -as [int]};Label="Free Size - GB"} `
    | ConvertTo-html | Set-CellColor Freespace red -Filter "Freespace -lt 20000000000" | Set-CellColor Freespace green -Filter "Freespace -gt 30000000000" `
    | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

Get-WmiObject win32_DiskDrive -ComputerName $vComputerName | Select-Object Model,SerialNumber,Description,MediaType,FirmwareRevision `
    | ConvertTo-html | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

Get-WmiObject -computername $vComputerName win32_processor | Measure-Object -property LoadPercentage -Average | Select-Object-object @{Name = "CPU Load"; Expression = {$_.Average}}  `
    | ConvertTo-html -Property 'CPU Load' -Fragment | Set-CellColor "CPU Load" red -Filter "CPU Load -gt 50" | Set-CellColor "CPU Load" green -Filter "CPU Load -lt 50" | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

Invoke-WmiMethod -ComputerName $vComputerName -Namespace "ROOT\ccm\ClientSDK" -Class "CCM_ClientUtilities" -Name DetermineIfRebootPending | Select-Object-Object -Property RebootPending `
     | ConvertTo-html -Property RebootPending -Fragment | Set-CellColor "RebootPending" red -Filter "RebootPending -eq 'True'" | Set-CellColor "RebootPending" green -Filter "RebootPending -eq 'False'" | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

get-wmiobject Win32_ReliabilityStabilityMetrics -computername $vComputerName -property @("__SERVER", "SystemStabilityIndex") | Select-Object-object -first 1 __SERVER,SystemStabilityIndex `
    | ConvertTo-html -Body "<H2>Reliability Index </H2>" | Set-CellColor "SystemStabilityIndex" red -Filter "SystemStabilityIndex -lt 8" | Set-CellColor "SystemStabilityIndex" green -Filter "SystemStabilityIndex -gt 8" | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

    
################################################
#  Process Information
#################################################

Get-Counter -ComputerName $vComputerName '\Process(*)\% Processor Time' `
    | Select-Object-Object -ExpandProperty countersamples `
    | Select-Object-Object -Property instancename, cookedvalue `
    | Sort-Object -Property cookedvalue -Descending | Select-Object-Object -First 10 InstanceName,@{L='CPU';E={($_.Cookedvalue/100).toString('P')}} `
    | ConvertTo-html -Body "<H2> Current Process Information </H2>" | Set-CellColor "CPU" red -Filter "CPU -gt 80" | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

Get-WmiObject win32_process -ComputerName $vComputerName | Select-Object Caption,ProcessId,@{Expression={$_.Vm /1mb -as [Int]};Label="VM (MB)"},@{Expression={$_.Ws /1Mb -as [Int]};Label="WS (MB)"} |Sort-Object "WS (MB)" -Descending | Select-Object -first 15 `
    | ConvertTo-html  -Head $a -Body "<H2> Process Memory Usage </H2>" | Set-CellColor "WS (MB)" red -Filter "WS (MB) -gt 150" | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

$sw.Stop()
write-host "Elapsed Time " $sw.Elapsed.TotalSeconds "seconds" -ForegroundColor Black -BackgroundColor Green


################################################
#  Power Plan
################################################

write-host "Getting Power Plan" -ForegroundColor Yellow -BackgroundColor DarkBlue
$sw = [Diagnostics.Stopwatch]::StartNew()

Get-WmiObject -computername $vComputerName -Class win32_powerplan -Namespace root\cimv2\power | Select-Object-Object Description, ElementName, IsActive  `
    | ConvertTo-html -Body "<H2>Power Plan Settings </H2>" `
    | Set-CellColor IsActive green -Filter "IsActive -eq 'True'" `
    | Out-File -append "$filepath\$vComputerName.html" -ErrorAction SilentlyContinue
$sw.Stop()
write-host "Elapsed Time " $sw.Elapsed.TotalSeconds "seconds" -ForegroundColor Black -BackgroundColor Green


write-host "Getting Network Adapter Information"  -ForegroundColor Yellow -BackgroundColor DarkBlue
$sw = [Diagnostics.Stopwatch]::StartNew()

Invoke-Command -cn $vComputerName -scriptblock {Get-NetAdapter} | Select-Object MacAddress, LinkSpeed, MediaConnectionState, ifOperStatus, ifAlias, ActiveMaximumTransmissionUnit, DriverVersion, DriverInformation |
    ConvertTo-html -Body "<H2>Network Adapters </H2>" | Set-CellColor "ifOperStatus" green -Filter "ifOperStatus -like 'UP'" | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

#Invoke-Command -cn $vComputerName -scriptblock {Get-NetAdapterAdvancedProperty} | Select-Object Name, DisplayName, DisplayValue RegistryValue |
#    ConvertTo-html -Body "<H2>Network Advanced Properties </H2>" >>  "$filepath\$vComputerName.html" 

Invoke-Command -cn $vComputerName -scriptblock {Get-NetAdapterPowerManagement} | Select-Object InterfaceDescription, Name, AllowComputerToTurnOffDevice, Select-ObjectiveSuspend, DeviceSleepOnDisconnect |
    ConvertTo-html -Body "<H2>Network Power Management Properties </H2>" | Set-CellColor "AllowComputerToTurnOffDevice" red -Filter "AllowComputerToTurnOffDevice -eq '2'" | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

#get-WmiObject win32_networkadapter -ComputerName $vComputerName | Select-Object Name,Manufacturer,Description ,AdapterType,Speed,MACAddress,NetConnectionID | Where-Object speed -ge 1 | Sort-Object speed -Descending `
#    | ConvertTo-html -Body "<H2> Network Adapters</H2>" >>  "$filepath\$vComputerName.html" 

Get-WmiObject Win32_NetworkAdapterConfiguration -ComputerName $vComputerName |
    Select-Object-Object Description, DHCPServer, 
        @{Name='IpAddress';Expression={$_.IpAddress -join '; '}}, 
        @{Name='IpSubnet';Expression={$_.IpSubnet -join '; '}}, 
        @{Name='DefaultIPgateway';Expression={$_.DefaultIPgateway -join '; '}}, 
        @{Name='DNSServerSearchOrder';Expression={$_.DNSServerSearchOrder -join '; '}}, 
        WinsPrimaryServer, WINSSecondaryServer| Where-Object-Object {$_.IpAddress -ge 1 } `
    | ConvertTo-html -Body "<H2>IP Address </H2>" >>  "$filepath\$vComputerName.html" 

Invoke-Command -cn $vComputerName -scriptblock {get-smbconnection} | Select-Object Credential, Dialect, Encrypted, NumOpens, Redirected, ServerName, ShareName, Signed |
    ConvertTo-html -Body "<H2>SMB Connections </H2>" | Set-CellColor "Dialect" red -Filter "Dialect -lt '3'" | Set-CellColor "Encrypted" red -Filter "Encrypted -eq 'False'" | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

$sw.Stop()
write-host "Elapsed Time " $sw.Elapsed.TotalSeconds "seconds" -ForegroundColor Black -BackgroundColor Green

################################################
#  Display HTTP Response Time
################################################
 $timeTaken = Measure-Command -Expression {
 $site = Invoke-WebRequest -Uri $URL1 -UseBasicParsing
 }
 $milliseconds = $timeTaken.TotalMilliseconds
 $milliseconds = [Math]::Round($milliseconds, 0)
 $Context.SetValue($milliseconds);
 write-host Website download time to $URL1 took -ForegroundColor Green 
 write-host $milliseconds Milliseconds -ForegroundColor Red


 $timeTaken = Measure-Command -ComputerName $vComputerName -Expression {
 $site = Invoke-WebRequest -Uri $URL2 -UseBasicParsing
 }
 $milliseconds = $timeTaken.TotalMilliseconds
 $milliseconds = [Math]::Round($milliseconds, 0)
 $Context.SetValue($milliseconds);
 write-host Website download time to $url2 took -ForegroundColor Green 
 write-host $milliseconds Milliseconds -ForegroundColor Red


################################################
#  AMT
################################################
write-host "Getting Intel AMT Information"  -ForegroundColor Yellow -BackgroundColor DarkBlue
										  
Invoke-Command -cn $vComputerName -ScriptBlock {Get-ChildItem "HKLM:\SOFTWARE\Intel\Setup and Configuration Software\SystemDiscovery" | ForEach-Object {Get-ItemProperty $_.pspath |Select-Object IsATEnabledInBios,SystemDataVersion,LastTimeUpdated,SCSVersion,BIOSVersion,AMTSKU,AMTVersion,FWVersion,MEIVersion,IsMEIEnabled,LMSVersion,SBAState}} `
    | ConvertTo-html  -Head $a -Body "<H2> INTEL AMT </H2>" >>  "$filepath\$vComputerName.html" -EA SilentlyContinue

Invoke-Command -cn $vComputerName -ScriptBlock {Get-ChildItem "HKLM:\SOFTWARE\Wow6432Node\Intel\Setup and Configuration Software\SystemDiscovery" | ForEach-Object {Get-ItemProperty $_.pspath | Select-Object IsATEnabledInBios,SystemDataVersion,LastTimeUpdated,SCSVersion,BIOSVersion,AMTSKU,AMTVersion,FWVersion,MEIVersion,IsMEIEnabled,LMSVersion,SBAState}} `
    | ConvertTo-html >>  "$filepath\$vComputerName.html" -EA SilentlyContinue

################################################
#  Reliability Information
#################################################
write-host "Getting Reliability Information" -ForegroundColor Yellow -BackgroundColor DarkBlue
$sw = [Diagnostics.Stopwatch]::StartNew()
ConvertTo-Html -Body "<H1>Reliability Information </H1>" >> "$filepath\$vComputerName.html"

#get-wmiobject Win32_ReliabilityRecords -computername $vComputerName -property Message | Select-Object-object -first 20 Message | format-list * | ConvertTo-html -Body "<H2>Last 20 Reliability Events </H2>" >>  "$filepath\$vComputerName.html" | Set-AlternatingRows -CSSEvenClass even -CSSOddClass odd
get-wmiobject Win32_ReliabilityStabilityMetrics -computername $vComputerName -property @("__SERVER", "SystemStabilityIndex") | Select-Object-object -first 1 __SERVER,SystemStabilityIndex `
    | ConvertTo-html -Body "<H2>Reliability Index </H2>" `
    | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue
    #| format-table 

get-wmiobject -computername $vComputerName Win32_ReliabilityRecords -property @("SourceName", "EventIdentifier") | Where-Object {!(($_.Sourcename -eq "Microsoft-Windows-WindowsUpdateClient" -or $_.Sourcename -eq "MsiInstaller" ))} | group-object -property SourceName,EventIdentifier -noelement | sort-object -descending Count | Select-Object-object Count,Name `
    | ConvertTo-html -Body "<H2>Reliability Distribution </H2>" `
    | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

get-wmiobject Win32_ReliabilityRecords -computername $vComputerName -property Message, TimeGenerated | Select-Object-object -Last 30 @{LABEL='Date-Time';EXPRESSION={$_.ConverttoDateTime($_.TimeGenerated)}}, Message | Where-Object {!(($_.Message -like "*successfully installed*" -or $_.Message -like "*error status: 0." -or $_.Message -like "*Windows Installer requires a system restart*" ))}  `
    | ConvertTo-html -Body "<H2>Reliability Messages </H2>" `
    | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue 

write-host "Getting detailed application hang information" -ForegroundColor Yellow -BackgroundColor DarkBlue

Get-WmiObject -computername $vComputerName -Class Win32_ReliabilityRecords -Filter "ProductName = 'iexplore.exe'" | Measure-Object | Select-Object-Object count `
    | ConvertTo-html -Body "<H2>Total IE Hangs </H2>" `
    | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue
Get-WmiObject -computername $vComputerName -Class win32_reliabilityRecords -Filter "ProductName = 'iexplore.exe'" | Foreach-Object {[regex]::matches($_.message, "Faulting module path: [a-zA-Z:0-9()\\\s]*")} | Sort-Object -Property value | Group-Object -Property value | Sort-Object -property count -Descending `
    | ConvertTo-html -Body "<H2>Hanging IE Modules </H2>" `
    | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue


Get-WmiObject -computername $vComputerName -Class Win32_ReliabilityRecords -Filter "ProductName = 'Outlook.exe'" | Measure-Object | Select-Object-Object count `
    | ConvertTo-html -Body "<H2>Total Outlook Hangs </H2>" `
    | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue
Get-WmiObject -computername $vComputerName -Class win32_reliabilityRecords -Filter "ProductName = 'Outlook.exe'" | Foreach-Object {[regex]::matches($_.message, "Faulting module path: [a-zA-Z:0-9()\\\s]*")} | Sort-Object -Property value | Group-Object -Property value | Sort-Object -property count -Descending `
    | ConvertTo-html -Body "<H2>Hanging Outlook Modules </H2>" `
    | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

$sw.Stop()
write-host "Elapsed Time " $sw.Elapsed.TotalSeconds "seconds" -ForegroundColor Black -BackgroundColor Green

################################################
#  Log Files
#################################################
write-host "Gathering Security Logs"  -ForegroundColor Yellow -BackgroundColor DarkBlue
$sw = [Diagnostics.Stopwatch]::StartNew()

ConvertTo-Html -Body "<H1>System Logfile Information </H1>" >> "$filepath\$vComputerName.html"

#Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Security"; StartTime = $date } | Where-Object {($_.EventID -eq "4800" -or $_.EventID -eq "4801" -or $_.EventID -eq "1100" -or $_.EventID -eq "4648"-or $_.EventID -eq "4608")} | Select-Object TimeCreated, ID, Message, ProviderName `
#   | ConvertTo-html  -Head $a -Body "<H2> Login Events </H2>" >> "$filepath\$vComputerName.html" -EA SilentlyContinue

#Get-EventLog -ComputerName $vComputerName -LogName Security -Newest 20000 | Where-Object {($_.EventID -eq "4800" -or $_.EventID -eq "4801" -or $_.EventID -eq "1100" -or $_.EventID -eq "4648"-or $_.EventID -eq "4608")} | Select-Object TimeGenerated, EventID, EntryType, Source, Message `
#Get-EventLog -ComputerName $vComputerName -LogName Security -after $date | Where-Object {(($_.EventID -eq "4800" -or $_.EventID -eq "4801" -or $_.EventID -eq "1100" -or $_.EventID -eq "4648"-or $_.EventID -eq "4608" -or $_.EventID -eq "4624" -or $_.EventID -eq "4634"))} | Select-Object TimeGenerated, EventID, EntryType, Source, Message `
#### Filter out events for the computer and user gathering info
$filterlogins = @("$vComputerName$", $vUserName)

$loginarray1 = $null
$loginarray1 = Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Security"; StartTime = $date; Data=$ComputerOwner.USERNAME } | Select-Object TimeCreated, ID, Message, ProviderName 
$loginarray2 = Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Security"; StartTime = $date; ID=1100 } | Select-Object TimeCreated, ID, Message, ProviderName 
$loginarray3 = Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Application"; StartTime = $date; ID=801 } | Select-Object TimeCreated, ID, Message, ProviderName 
$loginarray4 = Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "System"; StartTime = $date; ID=12 } | Select-Object TimeCreated, ID, Message, ProviderName 
$loginarray5 = Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "System"; StartTime = $date; ID=13 } | Select-Object TimeCreated, ID, Message, ProviderName 
$loginarray6 = Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Security"; StartTime = $date; ID=4800 } | Select-Object TimeCreated, ID, Message, ProviderName 
$loginarray7 = Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Security"; StartTime = $date; ID=4648 } | Select-Object TimeCreated, ID, Message, ProviderName 
#$loginarray8 = Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Security"; StartTime = $date; ID=4624 } | Select-Object TimeCreated, ID, Message, ProviderName 
#$loginarray9 = Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Security"; StartTime = $date; ID=4634 } | Select-Object TimeCreated, ID, Message, ProviderName 

write-host "Sorting Arrayed Logs"  -ForegroundColor Yellow -BackgroundColor DarkBlue
[array]$Userlogineventlogs = $null
$Userlogineventlogs += $loginarray1 
$Userlogineventlogs += $loginarray2
$Userlogineventlogs += $loginarray3
$Userlogineventlogs += $loginarray4
$Userlogineventlogs += $loginarray5
$Userlogineventlogs += $loginarray6
$Userlogineventlogs += $loginarray7
$Userlogineventlogs += $loginarray8
$Userlogineventlogs += $loginarray9

$Userlogineventlogs | Sort-Object TimeCreated -Descending | Where-Object {!(($_.Message -like "*$vComputerName$*" -or $_.Message -like "*UpdateTrustedSites.exe*" -or $_.Message -like "*Microsoft.Uev.SyncController.exe*" -or $_.Message -like "*A new process has been created*"))} | ConvertTo-html -Body "<H2>TimeLine For Login Events </H2>" `
    | Set-CellColor Id red -Filter "Id -eq 30804 -or Id -eq 30805 -or Id -eq 109 -or Id -eq 42 -or Id -eq 27 -or Id -eq 200 -or ID -eq 1100 -or ID -eq 4800" | Set-CellColor Message red -Filter "Message -like '*Exchange has been lost*'" | Set-CellColor Message red -Filter "Message -like '*entering sleep*'" | Set-CellColor Message red -Filter "Message -like '*Power source change*'" | Set-CellColor Message red -Filter "Message -like '*link is disconnected*'" | Set-CellColor Message red -Filter "Message -like '*shutdown*'" `
    | Set-CellColor Id green -Filter "Id -eq 30807 -or Id -eq 30805 -or Id -eq 30806 -or Id -eq 801 -or Id -eq 100 -or Id -eq 4801" | Set-CellColor Message green -Filter "Message -like '*re-established*'" | Set-CellColor Message green -Filter "Message -like '*started up*'" | Set-CellColor Message green -Filter "Message -like '*resumed from sleep*'" | Set-CellColor Message green -Filter "Message -like '*has returned*'" `
    | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue


$sw.Stop()
write-host "Elapsed Time " $sw.Elapsed.TotalSeconds "seconds" -ForegroundColor Black -BackgroundColor Green

#################################################
write-host "Getting Application Log Information" -ForegroundColor Yellow -BackgroundColor DarkBlue
$sw = [Diagnostics.Stopwatch]::StartNew()

Get-EventLog -ComputerName $vComputerName -LogName Application -EntryType Error,Warning | Where-Object {!(($_.Source -eq 'VSTO 4.0') -or ($_.Source -eq '.NET Runtime') -or ($_.Source -eq 'Microsoft-Windows-EFS') -or ($_.Source -eq 'SideBySide') -or ($_.Message -like "*Skipping:*")) -and ($_.TimeGenerated -gt $date )} | Select-Object TimeGenerated, EntryType, Source, Message | ConvertTo-html  -Head $a -Body "<H2> Applog Entries </H2>" >> "$filepath\$vComputerName.html"
#Get-Eventlog -ComputerName $vComputerName -LogName System -EntryType Error, Warning -Newest 75 | Select-Object TimeGenerated, EntryType, Source, Message | ConvertTo-html  -Head $a -Body "<H2> Newest 50 System Entries </H2>" >> "$filepath\$vComputerName.html"
#Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "System"; StartTime = $date } | Where-Object {$_.LevelDisplayName -eq "Critical" -or $_.LevelDisplayName -eq "Warning" -or $_.LevelDisplayName -eq "Error" } | Select-Object TimeCreated, ID, Message | ConvertTo-html  -Head $a -Body "<H2> Last 2 Days - System Event Log Entries </H2>" >> "$filepath\$vComputerName.html"

write-host "Getting System Log Information" -ForegroundColor Yellow -BackgroundColor DarkBlue        
Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "System"; StartTime = $date } | Where-Object {$_.LevelDisplayName -eq "Warning" -or $_.LevelDisplayName -eq "Critical" -or $_.LevelDisplayName -eq "Error" -or $_.ID -eq "26" } | Select-Object TimeCreated, ID, Message `
    | ConvertTo-html  -Head $a -Body "<H2> System Event Log Entries </H2>" | Set-CellColor ID red -Filter "ID -eq 6008 -or ID -eq 26 -or ID -eq 2004 -or ID -eq 1001 -or ID -eq 41 -or ID -eq 1085" | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

write-host "Getting Setup Log Information" -ForegroundColor Yellow -BackgroundColor DarkBlue        
Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Setup"; StartTime = $date } | Select-Object TimeCreated, ID, Message | ConvertTo-html  -Head $a -Body "<H2> Setup Log Entries </H2>" >> "$filepath\$vComputerName.html" -EA SilentlyContinue
Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Microsoft-Windows-Application Server-Applications/Operational"; StartTime = $date; ID = 57397 } | Select-Object TimeCreated, ID, Message | ConvertTo-html  -Head $a -Body "<H2> Application Server-Applications Log Entries </H2>" >> "$filepath\$vComputerName.html"
    ###Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Microsoft-Windows-CodeIntegrity/Operational"; StartTime = $date } | Select-Object TimeCreated, ID, Message | ConvertTo-html  -Head $a -Body "<H2> Last 2 Days - CodeIntegrity Log Entries </H2>" >> "$filepath\$vComputerName.html"

write-host "Getting Diagnostic-Performance Logs"
Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Microsoft-Windows-Diagnostics-Performance/Operational"; StartTime = $date } | Select-Object TimeCreated, ID, Message | ConvertTo-html  -Head $a -Body "<H2> Diagnostics-Performance Log Entries </H2>" >> "$filepath\$vComputerName.html"
Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Microsoft-Windows-GroupPolicy/Operational"; StartTime = $date} | Where-Object {$_.LevelDisplayName -eq "Warning" -or $_.LevelDisplayName -eq "Critical" -or $_.LevelDisplayName -eq "Error" -or $_.ID -eq "8005"-or $_.ID -eq "5016"-or $_.ID -eq "5312"-or $_.ID -eq "5017"} | Select-Object TimeCreated, ID, Message | Select-Object-Object -First 30 `
    | ConvertTo-html  -Head $a -Body "<H2> Latest 30 Items From Group Policy Log Entries </H2>" >> "$filepath\$vComputerName.html"

write-host "Getting NTFS Log Information"
Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Microsoft-Windows-Ntfs/WHC"; StartTime = $date } | Select-Object TimeCreated, ID, Message | ConvertTo-html  -Head $a -Body "<H2> Ntfs Log Entries </H2>" >> "$filepath\$vComputerName.html"
    ###Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Microsoft-Windows-SMBClient/Connectivity"; StartTime = $date } | Select-Object TimeCreated, ID, Message | ConvertTo-html  -Head $a -Body "<H2> Last 2 Days - SMBClient/Connectivity Log Entries </H2>" >> "$filepath\$vComputerName.html"
    ###Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Microsoft-Windows-SMBClient/Security"; StartTime = $date } | Select-Object TimeCreated, ID, Message | ConvertTo-html  -Head $a -Body "<H2> Last 2 Days - SMBClient/Security Log Entries </H2>" >> "$filepath\$vComputerName.html"
write-host "Getting User Profile Log Information"
Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Microsoft-Windows-User Profile Service/Operational"; StartTime = $date; ID = 67 } | Select-Object TimeCreated, ID, Message | ConvertTo-html  -Head $a -Body "<H2> User Profile Service Log Entries </H2>" >> "$filepath\$vComputerName.html"
Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Microsoft-Windows-Windows Defender/Operational"; StartTime = $date } | Select-Object TimeCreated, ID, Message | Select-Object-Object -First 30 `
    | ConvertTo-html  -Head $a -Body "<H2> Windows Defender Log Entries </H2>" >> "$filepath\$vComputerName.html"
write-host "Getting PowerBroker Events"
Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "System"; StartTime = $date } | Where-Object {($_.ProviderName -eq "Privman" ) -and ($_.ID -eq "28697" -or $_.ID -eq "28701") } | Select-Object TimeCreated, ID, Message | ConvertTo-html  -Head $a -Body "<H2> PowerBroker Events </H2>" >> "$filepath\$vComputerName.html" -EA SilentlyContinue
$sw.Stop()
write-host "Elapsed Time " $sw.Elapsed.TotalSeconds "seconds" -ForegroundColor Black -BackgroundColor Green


################################################
#  Updates and Hotfixes
################################################
write-host "Getting Hotfix and Updates Information"  -ForegroundColor Yellow -BackgroundColor DarkBlue
$sw = [Diagnostics.Stopwatch]::StartNew()

Get-HotFix -ComputerName $vComputerName | Select-Object Description, Hotfixid, InstalledOn | Sort-Object InstalledOn -Descending `
    | ConvertTo-html  -Head $a -Body "<H2> Installed Hotfixes </H2>" >>  "$filepath\$vComputerName.html"

#####################
Get-WmiObject -ComputerName $vComputerName -Class CCM_SoftwareUpdate -Filter ComplianceState=0 -Namespace root\CCM\ClientSDK | Select-Object-Object Name, ArticleID, @{LABEL='StartTime';EXPRESSION={$_.ConverttoDateTime($_.StartTime)}}, @{LABEL='Deadline';EXPRESSION={$_.ConverttoDateTime($_.Deadline)}} `
     | ConvertTo-html  -Head $a -Body "<H2> Pending Updates From SCCM </H2>" | Set-AlternatingRows -CSSEvenClass even -CSSOddClass odd `
     | out-file "$filepath\$vComputerName.html" -Append `
     | ConvertTo-html  -Head $a -Body "<H2> Pending Updates From SCCM </H2>" | Set-AlternateRows -CSSEvenClass even -CSSOddClass odd >>  "$filepath\$vComputerName.html"
#####################
    
Invoke-WmiMethod -ComputerName $vComputerName -Namespace "ROOT\ccm\ClientSDK" -Class "CCM_ClientUtilities" -Name DetermineIfRebootPending | Select-Object-Object -Property RebootPending `
     | ConvertTo-html -Property RebootPending -Fragment | Set-CellColor "RebootPending" red -Filter "RebootPending -eq 'True'" | Set-CellColor "RebootPending" green -Filter "RebootPending -eq 'False'" | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue
     

get-wmiobject -ComputerName $vComputerName -query "Select-Object * FROM CCM_UpdateStatus" -namespace "root\ccm\SoftwareUpdates\UpdatesStore" | Where-Object {$_.status -eq "Missing"} | Select-Object-Object -Property Status, Title  `
     | ConvertTo-html  -Head $a -Body "<H2> Missing Updates</H2>" >>  "$filepath\$vComputerName.html"

get-wmiobject -ComputerName $vComputerName -query "Select-Object * FROM CCM_UpdateStatus" -namespace "root\ccm\SoftwareUpdates\UpdatesStore" | Where-Object {($_.status -eq "Installed") -and !($_.title -like "*Defender*")} | Select-Object-Object -Property Status, Title `
     | ConvertTo-html  -Head $a -Body "<H2> Installed Updates</H2>" >>  "$filepath\$vComputerName.html"

write-host "Getting Updates Information"
Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Application"; StartTime = $date } | Where-Object {($_.ProviderName -eq "MsiInstaller")} | Select-Object TimeCreated, ID, Message `
    | ConvertTo-html  -Head $a -Body "<H2> MSI Installer Entries </H2>" | Set-CellColor ID green -Filter "ID -eq 1033 -or ID -eq 11728 -or ID -eq 1036 -or ID -eq 11707"| Set-CellColor ID red -Filter "ID -eq 11708 -or ID -eq 11935" | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "System"; StartTime = $date } | Where-Object {($_.ProviderName -eq "Microsoft-Windows-WindowsUpdateClient") -and ($_.ID -eq "19" -or $_.ID -eq "43") } | Select-Object TimeCreated, ID, Message | ConvertTo-html  -Head $a -Body "<H2> Windows Updates </H2>" | Set-CellColor ID green -Filter "ID -eq 19" | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue
#| ConvertTo-html  -Head $a -Body "<H2> Last 1 Days - System Event Log Entries </H2>" | Set-CellColor ID red -Filter "ID -eq 6008 -or ID -eq 26 -or ID -eq 2004 -or ID -eq 1001 -or ID -eq 41" | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

$sw.Stop()
write-host "Elapsed Time " $sw.Elapsed.TotalSeconds "seconds" -ForegroundColor Black -BackgroundColor Green

################################################
#  Startup Information
#################################################
write-host "Getting Driver and Startup Information" -ForegroundColor Yellow -BackgroundColor DarkBlue

ConvertTo-Html -Body "<H1>Startup and Software Information </H1>" >> "$filepath\$vComputerName.html"

Get-WmiObject win32_startupCommand -ComputerName $vComputerName | Select-Object Name,Location,Command,User,caption `
        | Sort-Object Name | ConvertTo-html  -Body "<H2>Startup Software</H2>" >>  "$filepath\$vComputerName.html"

Get-WmiObject win32_startupCommand -ComputerName $vComputerName | Measure-Object | Select-Object-Object Count `
        | ConvertTo-html >> "$filepath\$vComputerName.html"

Get-WmiObject win32_Service -ComputerName $vComputerName | Where-Object {$_.StartMode -eq "Auto" -and $_.State -eq "stopped"} |  Select-Object Name,StartMode,State `
        | ConvertTo-html  -Head $a -Body "<H2> Services </H2>" >>  "$filepath\$vComputerName.html"										 


################################################
#  Driver Information
#################################################
write-host "Getting Driver Information" -ForegroundColor Yellow -BackgroundColor DarkBlue

#ConvertTo-Html -Body "<H1>Drivers </H1>" >> "$filepath\$vComputerName.html"

Get-WmiObject Win32_PnPSignedDriver -ComputerName $vComputerName | Select-Object devicename, Signer, Manufacturer, driverversion, @{LABEL='DriverDate';EXPRESSION={$_.ConverttoDateTime($_.DriverDate)}} | Sort-Object devicename | Where-Object {!(($_.Signer -eq 'VSTO 4.0') -or ($_.devicename -eq 'Generic volume') -or ($_.devicename -eq 'Generic software device') -or ($_.devicename -eq 'Local Print Queue')-or ($_.Source -eq '.NET Runtime') -or ($_.Source -eq 'Microsoft-Windows-EFS') -or ($_.Manufacturer -eq $null) -or ($_.Manufacturer -eq "(Standard keyboards)") -or ($_.Manufacturer -eq "(Standard monitor types)") -or ($_.Manufacturer -eq "(Standard USB HUBs)") -or ($_.Manufacturer -eq "(Standard system devices)") -or ($_.Manufacturer -eq "(Standard disk drives)") -or ($_.Manufacturer -eq "Microsoft"))} `
    | ConvertTo-html  -Body "<H2>Driver Information</H2>" >>  "$filepath\$vComputerName.html" 


################################################
#  Installed Software
#################################################
write-host "Getting Installed Software" -ForegroundColor Yellow -BackgroundColor DarkBlue
$sw = [Diagnostics.Stopwatch]::StartNew()

Invoke-Command -cn $vComputerName -ScriptBlock {Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* | Select-Object-Object DisplayName, Publisher, InstallDate } | Where-Object {!(($_.DisplayName -eq $null))} | Sort-Object InstallDate -Descending `
        | ConvertTo-html -Head $a -Body "<H2> Installed Software </H2>" | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue
 
Invoke-Command -cn $vComputerName -ScriptBlock {Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* } | Where-Object {!(($_.DisplayName -eq $null))} | Measure-Object | Select-Object-Object Count `
        | ConvertTo-html | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

#Get-WmiObject -ComputerName $vComputerName -Class "win32_quickfixengineering" | Select-Object-Object -Property "Description", "HotfixID", @{Name="InstalledOn"; Expression={([DateTime]($_.InstalledOn)).ToLocalTime()}} | ConvertTo-html  -Head $a -Body "<H2> Installed Updates </H2>" >>  "$filepath\$vComputerName.html"
#"$vComputerName" | Get-WUHistory | Where-Object-object  {(($_.ResultCode -eq '2') -or ($_.ResultCode -eq '3') -or ($_.ResultCode -eq '1')-or ($_.ResultCode -eq '4') -and ($_.Title -notlike '*defender*'))} | Select-Object KB, Operation, ResultCode, HResult, Date, Title 

Get-WmiObject -ComputerName $vComputerName -Namespace root\ccm\SoftwareMeteringAgent -class CCM_RecentlyUsedApps | ForEach-Object {
[pscustomobject] @{ 
#AdditionalProductCodes=$_.AdditionalProductCodes
CompanyName=$_.CompanyName
ExplorerFileName=$_.ExplorerFileName
FileDescription=$_.FileDescription
#FilePropertiesHash=$_.FilePropertiesHash
FileSize=$_.FileSize
FileVersion=$_.FileVersion
FolderPath=$_.FolderPath
LastUsedTime= [System.Management.ManagementDateTimeConverter]::ToDateTime($_.LastUsedTime)
LastUserName=$_.LastUserName
LaunchCount=$_.LaunchCount
#msiDisplayName=$_.msiDisplayName
#msiPublisher=$_.msiPublisher
#msiVersion=$_.msiVersion
OriginalFileName=$_.OriginalFileName
#ProductCode=$_.ProductCode
#ProductLanguage=$_.ProductLanguage
ProductName=$_.ProductName
ProductVersion=$_.ProductVersion
#SoftwarePropertiesHash=$_.SoftwarePropertiesHash
#PSComputerName=$_.PSComputerName
} } | Sort-Object LastUsedTime -Descending | Select-Object-Object -First 50 | ConvertTo-html  -Head $a -Body "<H2> Last 50 Instances of Software Usage </H2>" `
    | Set-CellColor LaunchCount red -Filter "LaunchCount -gt 1000" `
    | Out-File -append "$filepath\$vComputerName.html" -EA SilentlyContinue

$sw.Stop()
write-host "Elapsed Time " $sw.Elapsed.TotalSeconds "seconds" -ForegroundColor Black -BackgroundColor Green

################################################
#  Kernel and Bugchecks
#################################################

Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Microsoft-Windows-Kernel-EventTracing/Admin"; StartTime = $date } | Select-Object TimeCreated, ID, Message | ConvertTo-html  -Head $a -Body "<H2> Kernel Event Log Entries </H2>" >> "$filepath\$vComputerName.html"

write-host "Getting Scheduled Task Events - COMMENTED OUT FOR NOW"
#Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "Microsoft-Windows-TaskScheduler/Operational"; StartTime = $date } | Select-Object TimeCreated, ID, Message | ConvertTo-html  -Head $a -Body "<H2> Last 2 Days - Scheduled Task History </H2>" >> "$filepath\$vComputerName.html"
#ConvertTo-Html -Body "<H1>BeyondTrust Events </H1>" >> "$filepath\$vComputerName.html"
write-host "Getting Dump Logs"

ConvertTo-Html -Body "<H1>Error Queue and Dump file and Bugcheck Information </H1>" >> "$filepath\$vComputerName.html"
Get-WinEvent -ComputerName $vComputerName -FilterHashTable @{ LogName = "System"; StartTime = $date } | Where-Object {($_.ProviderName -eq "Microsoft-Windows-WER-SystemErrorReporting") -or ($_.ProviderName -eq "Microsoft-Windows-Kernel-Power") -or ($_.ProviderName -eq "User32")} | Select-Object TimeCreated, ID, Message | ConvertTo-html  -Head $a -Body "<H2> Newest Bugchecks and Shutdown Events </H2>" >> "$filepath\$vComputerName.html" -EA SilentlyContinue
Get-ChildItem -Path \\$vComputerName\c$\ProgramData\Microsoft\Windows\WER\ReportQueue | Sort-Object -Property CreationTime -Descending |Select-Object CreationTime,Parent,FullName | ConvertTo-html  -Head $a -Body "<H2> C:\ProgramData\Microsoft\Windows\WER\ReportQueue Entries </H2>" >> "$filepath\$vComputerName.html"
Get-ChildItem \\$vComputerName\c$\Windows\MEMORY.DMP | Select-Object CreationTime,LastAccessTime,Parent,FullName | Sort-Object -Property LastAccessTime -Descending |Select-Object LastAccessTime,CreationTime,Parent,FullName |  ConvertTo-html  -Head $a -Body "<H2> Full memory dumps C:\Windows\MEMORY.DMP Entries </H2>" >> "$filepath\$vComputerName.html" -EA SilentlyContinue
Get-ChildItem \\$vComputerName\c$\Windows\minidump | Sort-Object -Property CreationTime -Descending | Select-Object CreationTime,Parent,FullName | ConvertTo-html  -Head $a -Body "<H2> MiniDump C:\Windows\minidump Entries </H2>" >> "$filepath\$vComputerName.html" -EA SilentlyContinue

$sw.Stop()
write-host "Elapsed Time " $sw.Elapsed.TotalSeconds "seconds" -ForegroundColor Black -BackgroundColor Green

#Get-Content $vComputerName\c$\windows\panther\setupact.log | Where-Object-Object { $_.Contains("ERROR") -or $_.Contains("Warning")} | ConvertTo-html  -Head $a -Body "<H2> c:\windows\panther\setupact.log Error Log Entries </H2>" >> "$filepath\$vComputerName.html"
##
##  Get flat log files and export to a text file
##
Remove-Item $filepath\$vComputerName.txt

write-host "Parsing DISM and CBS Logs - Commented out for now"
#$vComputerName | Out-file $filepath\$vComputerName.txt 
#Add-Content $filepath\$vComputerName.txt "`r`nwindows\panther\setupact.log`r`n " 
#Get-Content \\$vComputerName\c$\windows\panther\setupact.log | Where-Object-Object { $_.Contains("ERROR") -or $_.Contains("Warning")} | Out-File "$filepath\$vComputerName.txt" -Append
#Add-Content $filepath\$vComputerName.txt "`r`nwindows\panther\setuperr.log`r`n"
#Get-Content \\$vComputerName\c$\windows\panther\setuperr.log  | Out-File "$filepath\$vComputerName.txt" -Append
#Add-Content $filepath\$vComputerName.txt "`r`nwindows\panther\unattendgc\setupact.log`r`n"
#Get-Content \\$vComputerName\c$\windows\panther\unattendgc\setupact.log | Where-Object-Object { $_.Contains("ERROR") -or $_.Contains("Warning")} | Out-File "$filepath\$vComputerName.txt" -Append
#Add-Content $filepath\$vComputerName.txt "`r`nwindows\panther\unattendgc\setuperr.log`r`n"
#Get-Content \\$vComputerName\c$\windows\panther\unattendgc\setuperr.log | Out-File "$filepath\$vComputerName.txt" -Append


#Add-Content $filepath\$vComputerName.txt "`r`nwindows\logs\cbs\cbs.log`r`n"
#Get-Content \\$vComputerName\c$\windows\logs\cbs\cbs.log | Where-Object-Object { $_.Contains("corrupted") -or $_.Contains("ERROR") -or $_.Contains("Error") -or $_.Contains("Warning") -or $_.Contains("WARNING")} | Out-File "$filepath\$vComputerName.txt" -Append
#Add-Content $filepath\$vComputerName.txt "`r`nwindows\logs\dism\dism.log`r`n"
#Get-Content \\$vComputerName\c$\windows\logs\dism\dism.log | Where-Object-Object { $_.Contains("ERROR") -or $_.Contains("Warning")} | Out-File "$filepath\$vComputerName.txt" -Append
#Add-Content $filepath\$vComputerName.txt "`r`nwindows\ccm\logs\dcmagent.log`r`n"
#Get-Content \\$vComputerName\c$\windows\ccm\logs\dcmagent.log | Where-Object-Object { $_.Contains("Failed") -or $_.Contains("Warning")} | Out-File "$filepath\$vComputerName.txt" -Append
#Add-Content $filepath\$vComputerName.txt "`r`nwindows\ccm\logs\smsts.log`r`n"
#Get-Content \\$vComputerName\c$\windows\ccm\logs\smsts.log | Where-Object-Object { $_.Contains("Fail") -or $_.Contains("Warning")} | Out-File "$filepath\$vComputerName.txt" -Append

###################################

write-host "Run remote troubleshooting steps - Commented out for now" -ForegroundColor Green -Backgroundcolor DarkBlue

 
## Use the following each time your want to prompt the use
#$title = "Troubleshooting Packs" 
#$message = "Do you want to run the troubleshooting packs on the computer? If yes, you will need to enter your admin credentials."
#$result = $host.ui.PromptForChoice($title, $message, $options, 1)
#switch ($result) {
#    0{

#write-host "Running Remote Troubleshooting Packs" -ForegroundColor Green -BackgroundColor DarkBlue
#Import-Module -Name TroubleShootingPack
##Get-ChildItem C:\windows\diagnostics\system

#Enter-PSSession -ComputerName $vComputerName
#Import-Module -Name TroubleShootingPack
#Get-TroubleshootingPack -Path C:\windows\diagnostics\system\Video | Select-Object-Object -ExpandProperty RootCauses |Select-Object Name,Description,Resolution | ConvertTo-html  -Head $a -Body "<H2> Video Root Causes </H2>" >> "$filepath\$vComputerName.html"
#Get-TroubleshootingPack -Path C:\windows\diagnostics\system\Video | Invoke-TroubleshootingPack | ConvertTo-html  -Head $a -Body "<H2> Video Troubleshooting </H2>" >> "$filepath\$vComputerName.html"
#Get-TroubleshootingPack -Path C:\windows\diagnostics\system\IEBrowseWeb | Invoke-TroubleshootingPack
#Get-TroubleshootingPack -Path C:\windows\diagnostics\system\IESecurity | Invoke-TroubleshootingPack
#Get-TroubleshootingPack -Path C:\windows\diagnostics\system\Apps | Invoke-TroubleshootingPack
#Get-TroubleshootingPack -Path C:\windows\diagnostics\system\Printer | Invoke-TroubleshootingPack
#Get-TroubleshootingPack -Path C:\windows\diagnostics\system\Power | Invoke-TroubleshootingPack
#Get-TroubleshootingPack -Path C:\windows\diagnostics\system\Search | Invoke-TroubleshootingPack
#Exit-PSSession

 #   }1{
 #       Write-Host "No"
 #   }2{
 #       Write-Host "Cancel"
 #   }
#}


$Report = "The Report is generated On  $(get-date) by $((Get-Item env:\username).Value) on computer $((Get-Item env:\Computername).Value)"
$Report  >> "$filepath\$vComputerName.html" 

###################################
###################################
##  Formatting

#set-cellcolor -InputObject "$filepath\$vComputerName.html" -Filter "devicename -like ""*Intel(R) HD Graphics 4600*""" -Color Red | Out-File "$filepath\$vComputerName-2.html"

###################################
write-host "Sending Email" -ForegroundColor Yellow -BackgroundColor DarkBlue
$reportpath = "$filepath\$vComputerName.html"
$reportpath2 = "$filepath\$vComputerName.txt"
$smtphost = "mail.nmrs.com"  
$from = "DesktopScript@nmrs.com"  
#$to = "allengineering@nelsonmullins.com" 
#$to = "will.bryan@nelsonmullins.com"
#$to = "will.bryan@nelsonmullins.com, robbie.marshall@nelsonmullins.com, steve.conrick@nelsonmullins.com, shawn.mcnamara@nelsonmullins.com" 
IF ([string]::IsNullOrWhitespace($to)){$to = ""} else {}
$timeout = "30" 


######################################################################################## 
#############################################Send Email################################# 

$att = new-object Net.Mail.Attachment($reportpath)
#$att2 = new-object Net.Mail.Attachment($reportpath2)
$subject = "System Information for $vComputerName "  
$body = "Logs and information for $computeruser computer crashes for the past $days day(s)"
$smtp= New-Object System.Net.Mail.SmtpClient $smtphost  
$msg = New-Object System.Net.Mail.MailMessage $from, $to, $subject, $body  
$msg.isBodyhtml = $true  
$msg.Attachments.Add($att)
#$msg.Attachments.Add($att2)
$smtp.send($msg)  
$att.Dispose()
$att2.Dispose()

#invoke-Expression "$filepath\$vComputerName.html"  

#################### END of SCRIPT ####################################

# Get End Time
$endDTM = (Get-Date)
# Echo Time elapsed
write-host "Elapsed Time: $(($endDTM-$startDTM)).Minutes" -ForegroundColor Red -BackgroundColor Yellow









